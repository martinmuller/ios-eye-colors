//
//  ColorFC.swift
//  eyeColors
//
//  Created by Martin Muller on 08.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import PeaceKit

import Swinject

class ColorFC: ColorfulFC {
    // MARK: Variables
    var transitionHandler: ColorFlowDelegateHandler? {
        get { return (ac as? ColorfulAC)?.transitionHandler }
        set { (ac as? ColorfulAC)?.transitionHandler = newValue }
    }
    
    // MARK: Initialization
    convenience required init(ac: PeaceACP) {
        self.init(name: "Color", ac: ac)
    }
    
    // MARK: Setup
    override func setupDI(_ di: Container) {
        // Register flow specific view model(s)
        register(vm: ColorFavoriteVM.self)
        register(vm: ColorDetailVM.self)
        register(vm: ColorDetailEditVM.self)
        
        register(model: ColorModel.self)
        register(model: CaptureModel.self)
    }
    
    // MARK: Configure Initial View Controller
    override func configureInitialVC(_ vc: PeaceVCP) {
        let favoriteVC = vc as! ColorFavoriteVC
        
        favoriteVC.onShowCaptureListVC = { [unowned self] colors in self.showCaptureListVC(colors: colors) }
        favoriteVC.onDidLoad = { [weak self] in self?.transitionHandler = ColorFlowDelegateHandler(navigationController: self?.baseFlow) }
        favoriteVC.onShowColorDetailVC = { [weak self] color in self?.showColorDetailVC(with: color) }
    }
    
    override func setupConfigurations() {
        registerConfigurationForVC(ColorDetailVC.name) { (vc, _, _, _) in
            let detailVC = vc as! ColorDetailVC
            
            detailVC.onGoBack = { [weak self] in self?.baseBackTo() }
            detailVC.onScrollOverTop = { [weak self] recognizer in self?.transitionHandler?.handlePan(recognizer) }
            detailVC.onShowColorDetailEditVC = { [weak self] color in self?.showColorDetailEditVC(color: color) }
        }
    }
    
    private func showCaptureListVC(colors: [UIColor]) {
        registerChildFlowController(di.resolve(CaptureFC.self, argument: baseFlow)!, fcName: CaptureFC.name)
        
        flowDiveIn(CaptureFC.name, vcName: CaptureListVC.name, configure: { (vc, _, _, _) in
            let captureListVC = vc as! CaptureListVC
            
            captureListVC.onScrollOverTop = { [weak self] recognizer in
                self?.transitionHandler?.handlePan(recognizer)
            }
            
            if let vm = captureListVC.viewModel as? CaptureListVMP {
                vm.colorList.value = colors
            }
        })
    }
    
    private func showColorDetailVC(with color: UIColor?) {
        let detailVC: ColorDetailVC = loadVC(ColorDetailVC.name)
        
        (detailVC.viewModel as? ColorDetailVMP)?.color.value = color
        
        baseDiveIn(detailVC)
    }
    
    private func showColorDetailEditVC(color: UIColor?) {
        let detailEditVC: ColorDetailEditVC = loadVC(ColorDetailEditVC.name)
        
        if let vm = detailEditVC.viewModel as? ColorDetailEditVMP { vm.color.value = color }
        detailEditVC.onGoBack = { [weak self] in self?.baseBackTo(false) }
        
        baseDiveIn(detailEditVC, animated: false)
    }
}
