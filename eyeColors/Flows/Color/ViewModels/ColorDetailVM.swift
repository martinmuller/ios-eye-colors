//
//  ColorDetailVM.swift
//  eyeColors
//
//  Created by Martin Muller on 16.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import Foundation

import PeaceKit
import Swinject

import RxCocoa
import RxSwift

// MARK: Public View Model Interface
protocol ColorDetailVMP: PeaceVMP {
    // MARK: Input
    var color: Variable<UIColor?> { get }
    var requestSaveColor: Driver<UIColor> { get set }
    
    // MARK: Output
    
    // MARK: Methods
}

class ColorDetailVM: ColorfulVM, ColorDetailVMP {
    // MARK: Enums
    
    // MARK: Constants
    
    // MARK: Input
    var color: Variable<UIColor?> = Variable(nil)
    var requestSaveColor: Driver<UIColor> = Driver.never()
    
    // MARK: Output
    
    // MARK: Public Methods
    override func cleanUp() {
        
    }
    
    // MARK: Protected Methods
    override func defineModels(_ di: Container) { models[ColorModel.name] = di.resolve(ColorModel.self) }
    
    override func bindOutputToModels(_: [String: PeaceModelP]) {

    }
    
    override func configureInput() {
        var modelColor = colorModel
        
        requestSaveColor.map { Color(color: $0) } ==> modelColor.requestSaveColor
    }
    
    // MARK: Private Methods
    
    // MARK: Syntax Sugar
    private var colorModel: ColorModelP { get { return models[ColorModel.name] as! ColorModelP } }
}

// Peace Rx Operators
infix operator -->; infix operator ==>; infix operator ~>; infix operator <->; infix operator +
