//
//  ColorDetailEditVM.swift
//  eyeColors
//
//  Created by Martin Muller on 26/10/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import PeaceKit
import Swinject

import RxCocoa
import RxSwift

// MARK: Public View Model Interface
protocol ColorDetailEditVMP: PeaceVMP {
    // MARK: Input
    var color: Variable<UIColor?> { get }
    
    // MARK: Output
    
    // MARK: Methods
}

class ColorDetailEditVM: ColorfulVM, ColorDetailEditVMP {
    // MARK: Enums
    
    // MARK: Constants
    
    // MARK: Input
    var color: Variable<UIColor?> = Variable(nil)
    
    // MARK: Output
    
    // MARK: Public Methods
    override func cleanUp() {
        
    }
    
    // MARK: Protected Methods
    override func defineModels(_ di: Container) { models[ColorModel.name] = di.resolve(ColorModel.self) }
    
    override func bindOutputToModels(_: [String: PeaceModelP]) {
        
    }
    
    override func configureInput() {
        var modelColor = colorModel
        
    }
    
    // MARK: Private Methods
    
    // MARK: Syntax Sugar
    private var colorModel: ColorModelP { get { return models[ColorModel.name] as! ColorModelP } }
}

// Peace Rx Operators
infix operator -->; infix operator ==>; infix operator ~>; infix operator <->; infix operator +
