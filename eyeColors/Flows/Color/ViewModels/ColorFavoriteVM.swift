//
//  ColorFavoriteVM.swift
//  eyeColors
//
//  Created by Martin Muller on 08.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import Foundation

import PeaceKit
import Swinject

import RxCocoa
import RxSwift

// MARK: Public View Model Interface
protocol ColorFavoriteVMP: PeaceVMP {
    // MARK: Input
    var requestStartCamera: Driver<Void> { get set }
    var requestStopCamera: Driver<Void> { get set }
    var requestZoomVelocity: Driver<CGFloat> { get set }
    var requestColorFromPosition: Driver<CGPoint> { get set }
    
    // MARK: Output
    var cameraOutput: Driver<UIImage> { get }
    var colorsFromOutput: Driver<[UIColor]> { get }
    var colorAtPosition: Driver<UIColor> { get }
    
    var resultColorList: Driver<ChangesetResult<Color>> { get }
    
    // MARK: Methods
}

class ColorFavoriteVM: ColorfulVM, ColorFavoriteVMP {
    // MARK: Enums
    
    // MARK: Constants
    
    // MARK: Input
    var requestStartCamera: Driver<Void> = Driver.never()
    var requestStopCamera: Driver<Void> = Driver.never()
    var requestZoomVelocity: Driver<CGFloat> = Driver.never()
    var requestColorFromPosition: Driver<CGPoint> = Driver.never()
    
    // MARK: Output
    private(set) var cameraOutput: Driver<UIImage> = Driver.never()
    private(set) var colorsFromOutput: Driver<[UIColor]> = Driver.never()
    private(set) var colorAtPosition: Driver<UIColor> = Driver.never()
    
    private(set) var resultColorList: Driver<ChangesetResult<Color>> = Driver.never()
    
    // MARK: Public Methods
    
    override func cleanUp() {
        
    }
    
    // MARK: Protected Methods
    override func defineModels(_ di: Container) {
        models[CaptureModel.name] = di.resolve(CaptureModel.self)
        models[ColorModel.name] = di.resolve(ColorModel.self)
    }
    
    override func bindOutputToModels(_: [String: PeaceModelP]) {
        captureModel.cameraOutput ==> cameraOutput
        captureModel.colorsFromOutput ==> colorsFromOutput
        captureModel.colorAtPosition ==> colorAtPosition
        
        colorModel.resultColorList ==> resultColorList
    }
    
    override func configureInput() {
        var modelCapture = captureModel
        
        requestStartCamera ==> modelCapture.requestStartCamera
        requestStopCamera ==> modelCapture.requestStopCamera
        requestZoomVelocity ==> modelCapture.requestZoomVelocity
        requestColorFromPosition ==> modelCapture.requestColorFromPosition
    }
    
    // MARK: Private Methods
    
    // MARK: Syntax Sugar
    //private func model() -> TaskModelP { return self.models[modelTask] as! TaskModelP }
    private var colorModel: ColorModelP { get { return models[ColorModel.name] as! ColorModelP } }
    private var captureModel: CaptureModelP { get { return models[CaptureModel.name] as! CaptureModelP } }
}

// Peace Rx Operators
infix operator -->; infix operator ==>; infix operator ~>; infix operator <->; infix operator +
