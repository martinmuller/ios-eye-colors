//
//  ColorFavoriteTableViewHeaderView.swift
//  eyeColors
//
//  Created by Martin Muller on 09.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

class ColorFavoriteTableViewHeaderView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet var viewBlur: UIVisualEffectView!
    @IBOutlet var viewHolder: UIView!
    @IBOutlet var lblTitle: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        Bundle.main.loadNibNamed(ColorFavoriteTableViewHeaderView.name, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        layoutIfNeeded()
    }
}
