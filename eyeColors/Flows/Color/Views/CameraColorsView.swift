//
//  CameraColorsView.swift
//  eyeColors
//
//  Created by Martin Muller on 10.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

class CameraColorsView: UIView {
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var viewCameraButton: UIView!
    
    private let colorSpaceBetweenMargin: CGFloat = 20
    private let colorSpaceEndMargin: CGFloat = 24
    private lazy var colorSize = { (UIScreen.main.bounds.width - ((colorSpaceEndMargin * 2) + (colorSpaceBetweenMargin * 4))) / 5 }()
    
    lazy var color1: ColorButton = { return ColorButton(frame: CGRect(x: 24, y: 40, width: colorSize, height: colorSize)) }()
    lazy var color2: ColorButton = { return ColorButton(frame: CGRect(x: 24 + (1 * colorSize) + (1 * colorSpaceBetweenMargin), y: 40, width: colorSize, height: colorSize)) }()
    lazy var color3: ColorButton = { return ColorButton(frame: CGRect(x: 24 + (2 * colorSize) + (2 * colorSpaceBetweenMargin), y: 40, width: colorSize, height: colorSize)) }()
    lazy var color4: ColorButton = { return ColorButton(frame: CGRect(x: 24 + (3 * colorSize) + (3 * colorSpaceBetweenMargin), y: 40, width: colorSize, height: colorSize)) }()
    lazy var color5: ColorButton = { return ColorButton(frame: CGRect(x: 24 + (4 * colorSize) + (4 * colorSpaceBetweenMargin), y: 40, width: colorSize, height: colorSize)) }()
    
    let btnCamera = CameraButton()
    
    var onCaptureColors: ([UIColor]) -> () = { _ in }
    var onColorSelected: (ColorButton?) -> () = { _ in }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        Bundle.main.loadNibNamed(CameraColorsView.name, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        layoutIfNeeded()
        
        viewCameraButton.addSubview(btnCamera)
        btnCamera.center(in: viewCameraButton)
        
        contentView.addSubviews(color1, color2, color3, color4, color5)
        
        btnCamera.onTouch = { [unowned self] in
            var colorsFinal: [UIColor] = []
            if let color = self.color1.backgroundColor { colorsFinal.append(color) }
            if let color = self.color2.backgroundColor { colorsFinal.append(color) }
            if let color = self.color3.backgroundColor { colorsFinal.append(color) }
            if let color = self.color4.backgroundColor { colorsFinal.append(color) }
            if let color = self.color5.backgroundColor { colorsFinal.append(color) }
            self.onCaptureColors(colorsFinal)
        }
        
        color1.onTouch = { [weak self] in self?.onColorSelected(self?.color1) }
        color2.onTouch = { [weak self] in self?.onColorSelected(self?.color2) }
        color3.onTouch = { [weak self] in self?.onColorSelected(self?.color3) }
        color4.onTouch = { [weak self] in self?.onColorSelected(self?.color4) }
        color5.onTouch = { [weak self] in self?.onColorSelected(self?.color5) }
    }
    
    public func handle(colors: [UIColor]) {
        color1.setBackgroundColor(colors[0])
        color2.setBackgroundColor(colors[1])
        color3.setBackgroundColor(colors[2])
        color4.setBackgroundColor(colors[3])
        color5.setBackgroundColor(colors[4])
    }
}
