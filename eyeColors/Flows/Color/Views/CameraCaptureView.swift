//
//  CameraCaptureView.swift
//  eyeColors
//
//  Created by Martin Muller on 09.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

class CameraCaptureView: UIView {
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var imgCamera: UIImageView!
    private var viewVisualEffect: UIVisualEffectView?
    private lazy var viewTrash = { return CameraTrashView(frame: CGRect(x: 0, y: -100, width: 209, height: 100)) }()
    
    var onPinchChanged: (CGFloat) -> () = { _ in }
    var onPositionSelected: (CGPoint) -> () = { _ in }
    
    private var touchLast: CGPoint?
    private let colorSelected = ColorButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        Bundle.main.loadNibNamed(CameraCaptureView.name, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        layoutIfNeeded()
        
        let gesturePinch = UIPinchGestureRecognizer(target: self, action: #selector(handlePinch(recognizer:)))
        gesturePinch.delaysTouchesBegan = true
        gesturePinch.cancelsTouchesInView = true
        imgCamera.addGestureRecognizer(gesturePinch)
        
        viewTrash.center.x = center.x
        contentView.addSubview(viewTrash)
        
        let gesturePan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:)))
        gesturePan.delaysTouchesBegan = true
        gesturePan.cancelsTouchesInView = true
        colorSelected.addGestureRecognizer(gesturePan)
        contentView.addSubview(colorSelected)
        
        enableBlur()
    }
    
    public func handle(image: UIImage?) {
        imgCamera.image = image
        disableBlur()
    }
    
    public func enableBlur() {
        generateViewVisualEffectIfNeeded()
    }
    
    public func disableBlur() {
        if viewVisualEffect != nil {
            UIView.animate(withDuration: 1.2, animations: { [weak self] in
                self?.viewVisualEffect?.effect = nil
            }) { [weak self] _ in
                self?.viewVisualEffect?.removeFromSuperview()
                self?.viewVisualEffect = nil
            }
        }
    }
    
    public func handle(color: UIColor) {
        UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .allowUserInteraction, animations: { [weak self] in
            self?.colorSelected.transform = .identity
            self?.colorSelected.alpha = 1.0
            self?.colorSelected.backgroundColor = color
            self?.colorSelected.center = self?.touchLast ?? CGPoint(x: 0, y: 0)
        }) { _ in }
    }
    
    private func generateViewVisualEffectIfNeeded() {
        if viewVisualEffect == nil {
            viewVisualEffect = UIVisualEffectView(effect: UIBlurEffect(style: .light))
            viewVisualEffect?.isUserInteractionEnabled = false
            contentView.insertSubview(viewVisualEffect!, aboveSubview: imgCamera)
            viewVisualEffect!.frame = contentView.frame
        }
    }
    
    @objc private func handlePinch(recognizer: UIPinchGestureRecognizer) {
        if recognizer.state == .changed {
            onPinchChanged(recognizer.velocity)
        }
    }
    
    @objc private func handlePan(recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: contentView)
        
        switch recognizer.state {
        case .began:
            viewTrash.show()
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .allowUserInteraction, animations: { [unowned self] in
                self.colorSelected.transform = self.colorSelected.transform.scaledBy(x: 1.3, y: 1.3)
            })
        case .changed:
            UIView.animate(withDuration: 0.1, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .allowUserInteraction, animations: { [unowned self] in
                self.colorSelected.center = CGPoint(x: self.colorSelected.center.x + translation.x, y: self.colorSelected.center.y + translation.y)
            })
            
            if colorSelected.frame.intersects(viewTrash.frame) { viewTrash.enable() }
            else { viewTrash.disable() }
            
            recognizer.setTranslation(.zero, in: contentView)
        case .ended:
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .allowUserInteraction, animations: { [weak self] in
                self?.colorSelected.transform = .identity
            })
            
            if colorSelected.frame.intersects(viewTrash.frame) {
                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .allowUserInteraction, animations: { [weak self] in
                    self?.colorSelected.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
                    self?.colorSelected.alpha = 0.0
                }) { [unowned self] _ in
                    self.viewTrash.hide()
                }
            }
            else {
                viewTrash.hide()
                
                guard let imagePosition = toImagePoint(viewPosition: colorSelected.center) else { return }
                touchLast = colorSelected.center
                onPositionSelected(imagePosition)
            }
        default: ()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: imgCamera)
            touchLast = position
            
            guard let imagePosition = toImagePoint(viewPosition: position) else { return }
            onPositionSelected(imagePosition)
        }
    }
    
    private func toImagePoint(viewPosition: CGPoint) -> CGPoint? {
        guard let currentImage = imgCamera.image else { return nil }
        
        let percentX = viewPosition.x / imgCamera.frame.size.width
        let percentY = viewPosition.y / imgCamera.frame.size.height
        
        let imagePosition = CGPoint(x: currentImage.size.width * percentX, y: currentImage.size.height * percentY)
        
        return imagePosition
    }
}
