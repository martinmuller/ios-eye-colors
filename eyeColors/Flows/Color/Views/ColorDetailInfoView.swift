//
//  ColorDetailInfoView.swift
//  eyeColors
//
//  Created by Martin Muller on 03/11/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

class ColorDetailInfoView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet var btnFavorite: UIButton!
    @IBOutlet var btnAddToCollection: UIButton!
    @IBOutlet var btnClose: UIButton!
    
    @IBOutlet var constraintLblHexLeft: NSLayoutConstraint!
    @IBOutlet var lblHex: UILabel!
    
    private lazy var animator = resolveAnimator()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    private func setupUI() {
        backgroundColor = .clear
        Bundle.main.loadNibNamed(ColorDetailInfoView.name, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        layoutIfNeeded()
        
        contentView.layer.cornerRadius = 40
        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        btnFavorite.roundCorners(12)
        btnAddToCollection.roundCorners(12)
    }
    
    private func resolveAnimator() -> UIViewPropertyAnimator {
        let animator = UIViewPropertyAnimator(duration: 0.5, curve: .linear)
        
        animator.addAnimations { [unowned self] in
            self.constraintLblHexLeft.constant = (self.contentView.frame.width / 2) - (self.lblHex.frame.width / 2)
            self.layoutIfNeeded()
        }
        
        animator.pausesOnCompletion = true
        
        return animator
    }
    
    public func handle(fractionComplete: CGFloat) {
        animator.fractionComplete = fractionComplete
    }
}
