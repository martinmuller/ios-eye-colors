//
//  CameraTrashView.swift
//  eyeColors
//
//  Created by Martin Muller on 12.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

class CameraTrashView: UIView {
    @IBOutlet private var contentView: UIView!
    @IBOutlet var lblTitle: UILabel!
    
    private let generatorLight = UIImpactFeedbackGenerator(style: .light)
    private var isDisabled = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        Bundle.main.loadNibNamed(CameraTrashView.name, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        layoutIfNeeded()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.cornerRadius = 20
    }
    
    public func show() {
        UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .allowUserInteraction, animations: { [unowned self] in
            self.frame = CGRect(x: self.frame.minX, y: -20, width: self.frame.width, height: self.frame.height)
        })
    }
    
    public func hide() {
        UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .allowUserInteraction, animations: { [unowned self] in
            self.frame = CGRect(x: self.frame.minX, y: -100, width: self.frame.width, height: self.frame.height)
        })
    }
    
    public func enable() {
        if isDisabled {
            generatorLight.impactOccurred()
            
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.lblTitle.text = "Drop".localized
            }
        }
        
        isDisabled = false
    }
    
    public func disable() {
        if !isDisabled {
            generatorLight.impactOccurred()
            
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.lblTitle.text = "Drag to dismiss".localized
            }
        }
        
        isDisabled = true
    }
}
