//
//  ColorDetailInfoCell.swift
//  eyeColors
//
//  Created by Martin Muller on 25/10/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

class ColorDetailInfoCell: UITableViewCell {
    @IBOutlet var viewMain: UIView!
    @IBOutlet var btnFavorite: UIButton!
    @IBOutlet var btnAddToCollection: UIButton!
    @IBOutlet var btnClose: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewMain.layer.cornerRadius = 40
        viewMain.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        btnFavorite.roundCorners(12)
        btnAddToCollection.roundCorners(12)
    }
}
