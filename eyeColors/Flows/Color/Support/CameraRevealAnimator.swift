//
//  CameraRevealAnimator.swift
//  eyeColors
//
//  Created by Martin Muller on 09.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

enum State {
    case close
    case open
}

extension State {
    var opposite: State {
        switch self {
        case .open: return .close
        case .close: return .open
        }
    }
}

class CameraRevealAnimator {
    // MARK: Running variables
    private(set) var currentState: State = .close
    
    private var runningAnimators = [UIViewPropertyAnimator]()
    private var animationProgress = [CGFloat]()
    private lazy var popupOffset: CGFloat = { return (UIApplication.shared.keyWindow?.safeAreaLayoutGuide.layoutFrame.size.height ?? 0) - 200 }()
    
    private let generatorLight = UIImpactFeedbackGenerator(style: .light)
    private let generatorMedium = UIImpactFeedbackGenerator(style: .medium)
    private let generatorHeavy = UIImpactFeedbackGenerator(style: .heavy)
    
    private lazy var panRecognizer: UIPanGestureRecognizer = {
        let recognizer = UIPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(popupViewPanned(recognizer:)))
        return recognizer
    }()
    
    private var originalTouchPoint: CGPoint = .zero
    
    // MARK: Variables:
    private let favoriteVC: ColorFavoriteVC!
    
    // MARK: Callbacks
    var onWillOpen: VoidBlock = { }
    var onWillClose: VoidBlock = { }
    
    // MARK: Setup
    init(favoriteVC: ColorFavoriteVC) {
        self.favoriteVC = favoriteVC
        
        favoriteVC.viewFavorite.addGestureRecognizer(panRecognizer)
        
        performAnimationCameraColors(to: .close)
    }
    
    // MARK: Animator
    private func animateTransitionIfNeeded(to state: State, duration: TimeInterval = 0.5) {
        guard runningAnimators.isEmpty else { return }
        
        let dampingRation: CGFloat = 0.8
        
        let transitionAnimator = UIViewPropertyAnimator(duration: duration, timingParameters: UISpringTimingParameters(dampingRatio: dampingRation, initialVelocity: CGVector(dx: 0, dy: 1)))
//        transitionAnimator.isInterruptible = true
        
        transitionAnimator.addAnimations {
            self.setConstraintLayout(for: state)
        }
        
        transitionAnimator.addAnimations {
            UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: [.allowUserInteraction, .calculationModeCubic], animations: {
                UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: state == .open ? 0.5 : 0.25, animations: { [weak self] in
                    switch state {
                        case .close: self?.performAnimationCameraColors(to: state)
                        case .open: self?.performAnimationBackground(to: state)
                    }
                })
                
                UIView.addKeyframe(withRelativeStartTime: 0.25, relativeDuration: 0.25, animations: { [weak self] in
                    switch state {
                        case .close: self?.performAnimationOverlayColors(to: state)
                        case .open: self?.performAnimationBlur(to: state)
                    }
                })
                
                UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.25, animations: { [weak self] in
                    switch state {
                        case .close: self?.performAnimationBlur(to: state)
                        case .open: self?.performAnimationOverlayColors(to: state)
                    }
                })
                
                UIView.addKeyframe(withRelativeStartTime: 0.75, relativeDuration: state == .close ? 0.5 : 0.25, animations: { [weak self] in
                    switch state {
                        case .close: self?.performAnimationBackground(to: state)
                        case .open: self?.performAnimationCameraColors(to: state)
                    }
                })
            })
        }
        
        transitionAnimator.addCompletion { [weak self] position in
            switch position {
            case .start:
                self?.currentState = state.opposite
                self?.setConstraintLayout(for: state.opposite)
            case .end:
                self?.currentState = state
                self?.setConstraintLayout(for: state)
            case .current: () }
            
            switch self?.currentState {
            case .open?: self?.onWillOpen()
            case .close?:
                self?.onWillClose()
                self?.favoriteVC.viewCamera.enableBlur()
            case .none: self?.onWillClose()
            }
            
            if position == .start {
                UIView.animate(withDuration: 0.2, animations: {
                    self!.performAnimationBackground(to: self!.currentState)
                    self!.performAnimationBlur(to: self!.currentState)
                    self!.performAnimationOverlayColors(to: self!.currentState)
                    self!.performAnimationCameraColors(to: self!.currentState)
                })
            }
            
            self?.runningAnimators.removeAll()
        }
        
        transitionAnimator.startAnimation()
        runningAnimators.append(transitionAnimator)
    }
    
    private func setConstraintLayout(for state: State) {
        switch state {
            case .close:
                favoriteVC.constraintFavoriteTop.constant = 0
            case .open:
                favoriteVC.constraintFavoriteTop.constant = popupOffset
        }
        
        favoriteVC.view.layoutIfNeeded()
    }
    
    private func performAnimationBackground(to state: State) {
        switch state {
        case .close:
            favoriteVC.viewShift.alpha = 1.0
        case .open:
            favoriteVC.viewShift.alpha = 0.0
        }
    }
    
    private func performAnimationBlur(to state: State) {
        switch state {
        case .close:
            favoriteVC.viewCamera.enableBlur()
        case .open: ()
//            favoriteVC.viewCamera.disableBlur()
        }
    }
    
    private func performAnimationOverlayColors(to state: State) {
        switch state {
        case .close:
            favoriteVC.viewBlur.effect = UIBlurEffect(style: .light)
            favoriteVC.viewTableHeader.viewBlur.effect = UIBlurEffect(style: .light)
            favoriteVC.viewTableHeader.backgroundColor = .clear
            favoriteVC.viewTableHeader.viewHolder.backgroundColor = .black
            favoriteVC.viewTableHeader.lblTitle.alpha = 1.0
            favoriteVC.viewFavorite.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        case .open:
            favoriteVC.viewBlur.effect = UIBlurEffect(style: .dark)
            favoriteVC.viewTableHeader.viewBlur.effect = UIBlurEffect(style: .dark)
            favoriteVC.viewTableHeader.backgroundColor = .black
            favoriteVC.viewTableHeader.viewHolder.backgroundColor = .white
            favoriteVC.viewTableHeader.lblTitle.alpha = 0.0
            favoriteVC.viewFavorite.backgroundColor = .black
        }
    }
    
    private func performAnimationCameraColors(to state: State) {
        let frameWidth = favoriteVC.view.frame.width
        
        switch state {
        case .close:
            favoriteVC.viewCameraColors.alpha = 0.0
            favoriteVC.viewCameraColors.color1.transform = CGAffineTransform(translationX: -(frameWidth / 2), y: 0).concatenating(CGAffineTransform(scaleX: 0.8, y: 0.8))
            favoriteVC.viewCameraColors.color2.transform = CGAffineTransform(translationX: -(frameWidth / 4), y: 0).concatenating(CGAffineTransform(scaleX: 0.8, y: 0.8))
            favoriteVC.viewCameraColors.color3.transform = CGAffineTransform(translationX: 0, y: frameWidth / 3).concatenating(CGAffineTransform(scaleX: 0.8, y: 0.8))
            favoriteVC.viewCameraColors.color4.transform = CGAffineTransform(translationX: frameWidth / 4, y: 0).concatenating(CGAffineTransform(scaleX: 0.8, y: 0.8))
            favoriteVC.viewCameraColors.color5.transform = CGAffineTransform(translationX: frameWidth / 2, y: 0).concatenating(CGAffineTransform(scaleX: 0.8, y: 0.8))
            
            favoriteVC.viewCameraColors.btnCamera.transform = CGAffineTransform(translationX: 0, y: frameWidth / 2).concatenating(CGAffineTransform(scaleX: 0.8, y: 0.8))
        case .open:
            favoriteVC.viewCameraColors.alpha = 1.0
            favoriteVC.viewCameraColors.color1.transform = .identity
            favoriteVC.viewCameraColors.color2.transform = .identity
            favoriteVC.viewCameraColors.color3.transform = .identity
            favoriteVC.viewCameraColors.color4.transform = .identity
            favoriteVC.viewCameraColors.color5.transform = .identity
            
            favoriteVC.viewCameraColors.btnCamera.transform = .identity
        }
    }
    
    // MARK: Recognizer
    @objc private func popupViewPanned(recognizer: UIPanGestureRecognizer) {
        let touchPoint = recognizer.location(in: favoriteVC.view)
        
        let shouldAnimateOverlay = ((recognizer.velocity(in: favoriteVC.view).y) > 0 && currentState == .close) || ((recognizer.velocity(in: favoriteVC.view).y) < 0 && currentState == .open)
        
        switch recognizer.state {
        case .began:
            shouldAnimateOverlay ? generatorLight.impactOccurred() : generatorHeavy.impactOccurred()
            
            animateTransitionIfNeeded(to: currentState.opposite)
            runningAnimators.forEach { $0.pauseAnimation() }
            animationProgress = runningAnimators.map { $0.fractionComplete }
            
            // Ruberbanding
            originalTouchPoint = touchPoint
        case .changed:
            var animating = false
            
            let translation = recognizer.translation(in: favoriteVC.viewFavorite)
            var fraction = translation.y / popupOffset
            
            if currentState == .open { fraction *= -1 }
            if runningAnimators[0].isReversed { fraction *= -1 }
            
            for (index, animator) in runningAnimators.enumerated() {
                if fraction >= 0 {
                    let fractionCompleted = fraction + animationProgress[index]
                    
                    if currentState == .open {
                        if fractionCompleted > 0.9 { onWillClose() }
                    }
                    else {
                        if fractionCompleted > 0.1 { onWillOpen() }
                    }
                    animator.fractionComplete = fractionCompleted
                    
                    animating = true
                }
                else {
                    animating = false
                }
            }

            // Ruberbanding
            if !animating {
                var offset = touchPoint.y - originalTouchPoint.y
                offset = offset > 0 ? pow(offset, 0.7) : -pow(-offset, 0.7)
                UIView.animate(withDuration: 0.1) { [weak self] in
                    self?.favoriteVC.viewFavorite.transform = CGAffineTransform(translationX: 0, y: offset)
                }
            }
        case .ended, .cancelled:
            generatorMedium.impactOccurred()
            
            let yVelocity = recognizer.velocity(in: favoriteVC.viewFavorite).y
            let shouldClose = yVelocity < 0
            
            if yVelocity == 0 {
                runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
                break }
            
            switch currentState {
            case .open:
                shouldAnimateOverlay ? onWillClose() : ()
                if !shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            case .close:
                shouldAnimateOverlay ? onWillOpen() : ()
                if shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if !shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } } }
            
            runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }

            // Ruberbanding
            let timingParameters = UISpringTimingParameters(damping: 0.6, response: 0.3)
            let animator = UIViewPropertyAnimator(duration: 0, timingParameters: timingParameters)
            animator.addAnimations { [weak self] in
                self?.favoriteVC.viewFavorite.transform = .identity
            }
            animator.isInterruptible = true
            animator.startAnimation()
        default: ()
        }
    }
}
