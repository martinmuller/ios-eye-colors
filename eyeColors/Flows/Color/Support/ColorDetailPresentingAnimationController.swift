//
//  ColorDetailPresentingAnimationController.swift
//  eyeColors
//
//  Created by Martin Muller on 18.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

typealias ColorDetailPresentingVC = ColorfulVC & ColorDetailPresentingP
protocol ColorDetailPresentingP {
    var selectedColor: UIView? { get }
}

class ColorDetailPresentingAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval { return 0.3 }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: .from) as? ColorDetailPresentingVC else { return }
        guard let toVC = transitionContext.viewController(forKey: .to) as? ColorDetailVC else { return }
        
        guard let fromView = transitionContext.view(forKey: .from) else { return }
        guard let toView = transitionContext.view(forKey: .to) else { return }
        
        guard let selectedColor = fromVC.selectedColor else { return }
        
        let container = transitionContext.containerView
        
        toView.alpha = 0.0
        container.addSubview(toView)
        
        let viewCircular = UIView(frame: selectedColor.positionIn(view: container))
        viewCircular.layer.cornerRadius = viewCircular.frame.height / 2
        viewCircular.backgroundColor = selectedColor.backgroundColor
        container.addSubview(viewCircular)
        
        UIView.animateKeyframes(withDuration: transitionDuration(using: transitionContext), delay: 0.0, options: .allowUserInteraction, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 1.0, animations: {
                viewCircular.transform = viewCircular.transform.scaledBy(x: 40, y: 40)
                
                toVC.constraintTabBarBottom.constant = 0
                toView.layoutIfNeeded()
            })
            UIView.addKeyframe(withRelativeStartTime: 0.25, relativeDuration: 0.75, animations: {
                toView.alpha = 1.0
            })
        }) { _ in
            let success = !transitionContext.transitionWasCancelled
            if !success {
                toView.removeFromSuperview()
            }
            
            viewCircular.removeFromSuperview()
            
            transitionContext.completeTransition(success)
        }
    }
}
