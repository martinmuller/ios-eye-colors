//
//  ColorFlowDelegateHandler.swift
//  eyeColors
//
//  Created by Martin Muller on 12.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

class ColorFlowDelegateHandler: NSObject, UINavigationControllerDelegate {
    // MARK: Variables
    private var interactionController: UIPercentDrivenInteractiveTransition?
    weak private var navigationController: UINavigationController?
    
    // MARK: Init
    override init() { super.init() }
    
    convenience init(navigationController: UINavigationController?) {
        self.init()
        self.navigationController = navigationController
        
        self.navigationController?.delegate = self
    }
    
    // MARK: Handle
    func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
        let percent = (gestureRecognizer.translation(in: gestureRecognizer.view!).y) / gestureRecognizer.view!.bounds.size.height
        
        if gestureRecognizer.state == .began {
            interactionController = UIPercentDrivenInteractiveTransition()
            navigationController?.popViewController(animated: true)
        } else if gestureRecognizer.state == .changed {
            interactionController?.update(percent)
        } else if gestureRecognizer.state == .ended {
            if percent > 0.35 && gestureRecognizer.state != .cancelled {
                interactionController?.finish()
            } else {
                interactionController?.cancel()
            }
            interactionController = nil
        }
    }
    
    // MARK: Delegate
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactionController
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if operation == .push && (toVC.nameRepresentation == CaptureListVC.name) {
            return CaptureListPresentingAnimationController()
        }
        else if operation == .push && toVC.nameRepresentation == ColorDetailVC.name {
            return ColorDetailPresentingAnimationController()
        }
        else if (fromVC.nameRepresentation == CaptureListVC.name) {
            return CaptureListPopingAnimationController()
        }
        else if fromVC.nameRepresentation == ColorDetailVC.name {
            return ColorDetailPopingAnimationController()
        }
        
        return nil
    }
}
