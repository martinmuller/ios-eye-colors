//
//  ColorDetailPopingAnimationController.swift
//  eyeColors
//
//  Created by Martin Muller on 18.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

class ColorDetailPopingAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval { return 0.5 }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: .from) as? ColorDetailVC else { return }
        guard let toVC = transitionContext.viewController(forKey: .to) as? ColorDetailPresentingVC else { return }
        
        guard let fromView = transitionContext.view(forKey: .from) else { return }
        guard let toView = transitionContext.view(forKey: .to) else { return }
        
        guard let selectedColor = toVC.selectedColor else { return }
        
        let container = transitionContext.containerView
        
        fromVC.viewColor.backgroundColor = .clear
        fromView.backgroundColor = .clear
        container.insertSubview(toView, belowSubview: fromView)
        
        let viewCircular = UIView(frame: selectedColor.positionIn(view: container))
        viewCircular.layer.cornerRadius = viewCircular.frame.height / 2
        viewCircular.backgroundColor = selectedColor.backgroundColor
        
        let desiredTranformScale = container.frame.height / (viewCircular.frame.height / 2)
        viewCircular.transform = viewCircular.transform.scaledBy(x: desiredTranformScale, y: desiredTranformScale)
        container.insertSubview(viewCircular, belowSubview: fromView)
        
        UIView.animateKeyframes(withDuration: transitionDuration(using: transitionContext), delay: 0.0, options: .allowUserInteraction, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 1.0, animations: {
                viewCircular.transform = .identity
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.85, animations: {
                fromVC.constraintTableViewTop.constant = container.frame.height
                fromVC.constraintTabBarBottom.constant = -container.frame.height
                fromView.layoutIfNeeded()
            })
        }) { _ in
            let success = !transitionContext.transitionWasCancelled
            if !success {
                toView.removeFromSuperview()
                fromVC.constraintTableViewTop.constant = 0
                fromVC.constraintTabBarBottom.constant = 0
                fromVC.viewColor.backgroundColor = selectedColor.backgroundColor
            }
            
            viewCircular.removeFromSuperview()
            
            transitionContext.completeTransition(success)
        }
    }
}
