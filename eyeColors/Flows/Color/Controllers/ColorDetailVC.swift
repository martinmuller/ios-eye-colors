//
//  ColorDetailVC.swift
//  eyeColors
//
//  Created by Martin Muller on 16.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

import Swinject
import PeaceKit

import RxSwift

class ColorDetailVC: ColorfulVC {
    // MARK: Outlets - Views
    @IBOutlet var tableView: UITableView!
    @IBOutlet var viewColor: UIView!
    
    // MARK: Outlets - Components (lbl = Label, view = View, btn = Button, fld = TextField, ..)
    @IBOutlet var constraintTableViewTop: NSLayoutConstraint!
    @IBOutlet var constraintTabBarBottom: NSLayoutConstraint!
    
    // MARK: Variables - UI
    private let viewHeader = ColorDetailInfoView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 255))
    
    // MARK: Constants
    private let actionSaveColor = Action<UIColor>()
    
    // MARK: Variables
    fileprivate var lastScrollViewOffsetY: CGFloat = 0
    fileprivate var cellAnimationList: [IndexPath : UIViewPropertyAnimator] = [:]
    
    fileprivate lazy var gesturePan = { return UIPanGestureRecognizer(target: self, action: #selector(handleViewPanned(recognizer:))) }()
    
    // MARK: Callbacks (on...)
    var onGoBack: VoidBlock = { }
    var onScrollOverTop: (UIPanGestureRecognizer) -> () = { _ in }
    var onShowColorDetailEditVC: (UIColor?) -> () = { _ in }
    
    // MARK: Local Actions (handle...)
//    @IBAction func handleSaveColor(_ sender: Any) {
//        guard let color = viewModel().color.value else { return }
//        actionSaveColor.trigger(with: color)
//    }
    
    // MARK: Navigation Actions (handle...)
    @objc private func handleViewPanned(recognizer: UIPanGestureRecognizer) {
        onScrollOverTop(recognizer)
    }

    @objc private func handleClose() {
        onGoBack()
    }
    
    // MARK: Internal Actions (do...)
    
    // MARK: Setup
    override func defineViewModel(_ di: Container) -> PeaceVMP { return di.resolve(ColorDetailVM.self)! }
    
    // MARK: Bind to View Model
    override func bindToViewModel(_ vm: PeaceVMP) {
        let bvm = vm as! ColorDetailVMP
        
        (bvm.color.asDriver() --> { [weak self] color in self?.viewColor.backgroundColor = color }) ~> rx.disposeBag
    }
    
    override func configureInput(_ vm: PeaceVMP) {
        var bvm = vm as! ColorDetailVMP
        
        actionSaveColor.outputDriver ==> bvm.requestSaveColor
    }
    
    // MARK: Bind local UI
    
    // MARK: Setup UI
    override func setupUI() {
        super.setupUI()
        
        print(navigationController?.navigationBar.frame)
        
        gesturePan.delegate = self
        view.addGestureRecognizer(gesturePan)
        
        tableView.register([ColorDetailInfoCell.name, ColorDetailComplementaryCell.name])
        tableView.contentInset = UIEdgeInsets(top: tableView.frame.height - 83 - viewHeader.frame.height, left: 0, bottom: 0, right: 0)
        tableView.tableHeaderView = viewHeader
    }
    
    // MARK: View Lifecycle
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    // MARK: Syntax Sugar
    private func viewModel() -> ColorDetailVMP { return self.viewModel as! ColorDetailVMP }
}

extension ColorDetailVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int { return 1 }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return 10 }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: ColorDetailComplementaryCell.name) as! ColorDetailComplementaryCell
            return cell
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == tableView else { return }
        
        let yPos = scrollView.contentOffset.y
        
        if yPos >= -100 && yPos <= 0 {
            viewHeader.handle(fractionComplete: 1 - (abs(yPos) / 100))
        }
    }
    
    private func doResolveAnimator<T: UITableViewCell>(for indexPath: IndexPath, cell: T) -> UIViewPropertyAnimator {
        if let animator = cellAnimationList[indexPath] {
            return animator
        }
        else {
            let animator = UIViewPropertyAnimator(duration: 1, curve: .linear) {
                cell.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }
            
            animator.pausesOnCompletion = true
            
            animator.startAnimation()
            animator.pauseAnimation()
            
            cellAnimationList[indexPath] = animator
            return animator
        }
    }
}

extension ColorDetailVC: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        let gesture = (gestureRecognizer as! UIPanGestureRecognizer)
        let direction = gesture.velocity(in: view).y
        
        let y = constraintTableViewTop.constant
        if (y == 0 && tableView.contentOffset.y <= 0 && direction > 0) {
            tableView.isScrollEnabled = false
        } else {
            tableView.isScrollEnabled = true
        }
        
        return false
    }
}
