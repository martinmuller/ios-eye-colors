//
//  ColorFavoriteVC.swift
//  eyeColors
//
//  Created by Martin Muller on 08.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

import Swinject
import PeaceKit

import RxSwift

import RealmSwift

class ColorFavoriteVC: CaptureListPresentingVC, ColorDetailPresentingP {
    // MARK: Outlets - Views
    @IBOutlet var viewCamera: CameraCaptureView!
    @IBOutlet var viewFavorite: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var viewShift: ShiftView!
    @IBOutlet var viewBlur: UIVisualEffectView!
    
    // MARK: Outlets - Components (lbl = Label, view = View, btn = Button, fld = TextField, ..)
    @IBOutlet var constraintFavoriteTop: NSLayoutConstraint!
    
    // MARK: Variables - UI
    lazy var viewTableHeader = { ColorFavoriteTableViewHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 88)) }()
    lazy var viewCameraColors: CameraColorsView = { return CameraColorsView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 180)) }()
    private var colorFromOutputLegal = true
    var selectedColor: UIView?
    
    // MARK: Constants
    private var animator: CameraRevealAnimator?
    
    private let actionStartCamera = Action<Void>()
    private let actionStopCamera = Action<Void>()
    private let actionZoomVelocity = Action<CGFloat>()
    private let actionPositionSelected = Action<CGPoint>()
    
    // MARK: Variables
    fileprivate var colorList: AnyRealmCollection<Color>?
    
    // MARK: Callbacks (on...)
    var onShowCaptureListVC: ([UIColor]) -> () = { _ in }
    var onDidLoad: VoidBlock = { }
    var onShowColorDetailVC: (UIColor?) -> () = { _ in }
    
    // MARK: Local Actions (handle...)
    private func handle(colorListChangeset: ChangesetResult<Color>) {
        
    }
    
    // MARK: Navigation Actions (handle...)
    
    // MARK: Internal Actions (do...)
    
    // MARK: Setup
    override func defineViewModel(_ di: Container) -> PeaceVMP { return di.resolve(ColorFavoriteVM.self)! }
    
    // MARK: Bind to View Model
    override func bindToViewModel(_ vm: PeaceVMP) {
        let bvm = vm as! ColorFavoriteVM
        
        (bvm.cameraOutput --> { [weak self] image in self?.viewCamera.handle(image: image) }) ~> rx.disposeBag
        (bvm.colorsFromOutput --> { [unowned self] colors in if self.colorFromOutputLegal { self.viewCameraColors.handle(colors: colors) } }) ~> rx.disposeBag
        (bvm.colorAtPosition --> { [weak self] color in self?.viewCamera.handle(color: color) }) ~> rx.disposeBag
        
        (bvm.requestStopCamera --> { [weak self] _ in self?.colorFromOutputLegal = false }) ~> rx.disposeBag
        (bvm.requestStartCamera --> { [weak self] _ in self?.colorFromOutputLegal = true }) ~> rx.disposeBag
    }
    
    override func configureInput(_ vm: PeaceVMP) {
        let bvm = vm as! ColorFavoriteVM
        
        actionStartCamera.outputDriver ==> bvm.requestStartCamera
        actionStopCamera.outputDriver ==> bvm.requestStopCamera
        actionZoomVelocity.outputDriver ==> bvm.requestZoomVelocity
        actionPositionSelected.outputDriver ==> bvm.requestColorFromPosition
    }
    
    // MARK: Bind local UI
    
    // MARK: Setup UI
    override func setupUI() {
        super.setupUI()
        
        onDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        setupAnimator()
        setupShiftView()
        setupFavoriteView()
        
        tableView.roundCorners([.topLeft, .topRight], radius: 40)
//        tableView.layer.cornerRadius = 40
        tableView.tableFooterView = UIView()
        
        viewBlur.layer.cornerRadius = 40
        viewBlur.clipsToBounds = true
        
        viewCamera.onPinchChanged = { [weak self] velocity in
            self?.actionZoomVelocity.trigger(with: velocity)
        }
        
        viewCamera.onPositionSelected = { [weak self] position in
            self?.actionPositionSelected.trigger(with: position)
        }
        
        viewCameraColors.onCaptureColors = { [unowned self] colors in
            self.onShowCaptureListVC(colors)
        }
        
        viewCameraColors.onColorSelected = { [weak self] btnColor in
            self?.selectedColor = btnColor
            self?.onShowColorDetailVC(btnColor?.backgroundColor)
        }
    }
    
    private func setupShiftView() {
        viewShift.setColors([.red, .blue, .yellow, .orange])
        viewShift.startTimedAnimation()
    }
    
    private func setupAnimator() {
        animator = CameraRevealAnimator(favoriteVC: self)
        
        animator?.onWillOpen = { [weak self] in
            self?.setNeedsUpdateOfHomeIndicatorAutoHidden()
            self?.setNeedsStatusBarAppearanceUpdate()
            self?.actionStartCamera.trigger(with: ())
        }
        animator?.onWillClose = { [weak self] in
            self?.setNeedsUpdateOfHomeIndicatorAutoHidden()
            self?.setNeedsStatusBarAppearanceUpdate()
            self?.actionStopCamera.trigger(with: ())
        }
    }
    
    private func setupFavoriteView() {
        viewFavorite.layer.applySketchShadow(alpha: 0.1, blur: 10)
        viewFavorite.roundCorners([.topLeft, .topRight], radius: 40)
//        viewFavorite.layer.cornerRadius = 40
        
        viewFavorite.addSubview(viewCameraColors)
    }
    
    // MARK: View Lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        actionStartCamera.trigger()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        actionStopCamera.trigger()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override var prefersStatusBarHidden: Bool {
        return (animator?.currentState == .open) ? true : false
    }
    func prefersHomeIndicatorAutoHidden() -> Bool {
        return (animator?.currentState == .open) ? true : false
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    // MARK: Syntax Sugar
    //private func viewModel() -> TaskEditVMP { return self.viewModel as! TaskEditVMP }
}

extension ColorFavoriteVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int { return 1 }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat { return viewTableHeader.frame.height }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? { return viewTableHeader }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return 0 }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
}

extension ColorFavoriteVC {
    var color1: ColorButton? { return viewCameraColors.color1 }
    var color2: ColorButton? { return viewCameraColors.color2 }
    var color3: ColorButton? { return viewCameraColors.color3 }
    var color4: ColorButton? { return viewCameraColors.color4 }
    var color5: ColorButton? { return viewCameraColors.color5 }
}
