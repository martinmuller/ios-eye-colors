//
//  ColorDetailEditVC.swift
//  eyeColors
//
//  Created by Martin Muller on 26/10/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

import Swinject
import PeaceKit
import FlexColorPicker

import RxSwift

class ColorDetailEditVC: ColorfulVC {
    // MARK: Outlets - Views
    
    // MARK: Outlets - Components (lbl = Label, view = View, btn = Button, fld = TextField, ..)
    
    // MARK: Variables - UI
    
    // MARK: Constants
    
    // MARK: Variables
    
    // MARK: Callbacks (on...)
    var onGoBack: VoidBlock = { }
    
    // MARK: Navigation Actions (handle...)
    @IBAction func handleGoBack(_ sender: Any) { onGoBack() }
    
    // MARK: Internal Actions (do...)
    
    // MARK: Setup
    override func defineViewModel(_ di: Container) -> PeaceVMP { return di.resolve(ColorDetailEditVM.self)! }
    
    // MARK: Bind to View Model
    override func bindToViewModel(_ vm: PeaceVMP) {
        let bvm = vm as! ColorDetailEditVMP
        
        (bvm.color --> { [weak self] color in self?.view.backgroundColor = color }) ~> rx.disposeBag
    }
    
    override func configureInput(_ vm: PeaceVMP) {
        
    }
    
    // MARK: Bind local UI
    
    // MARK: Setup UI
    override func setupUI() {
        super.setupUI()
    
//        let t = DefaultColorPickerViewController()
//        t.view.frame = CGRect(x: 0, y: 200, width: 400, height: 400)
//        t.colorPreview.colorView.removeFromSuperview()
//        
//        addChild(t)
//        view.addSubview(t.view)
//        t.didMove(toParent: self)
    }
    
    // MARK: View Lifecycle
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    // MARK: Syntax Sugar
    private func viewModel() -> ColorDetailEditVMP { return self.viewModel as! ColorDetailEditVMP }
}
