//
//  CaptureFC.swift
//  eyeColors
//
//  Created by Martin Muller on 12.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import PeaceKit

import Swinject

class CaptureFC: ColorfulFC {
    // MARK: Variables
    var transitionHandler: ColorFlowDelegateHandler? {
        get { return (ac as? ColorfulAC)?.transitionHandler }
        set { (ac as? ColorfulAC)?.transitionHandler = newValue }
    }
    
    // MARK: Initialization
    convenience required init(ac: PeaceACP) {
        self.init(name: "Capture", ac: ac)
    }
    
    // MARK: Setup
    override func setupDI(_ di: Container) {
        // Register flow specific view model(s)
        register(vm: CaptureListVM.self)
    }
    
    // MARK: Configure Initial View Controller
    override func configureInitialVC(_ vc: PeaceVCP) {

    }
    
    override func setupConfigurations() {
        registerConfigurationForVC(CaptureListVC.name) { (vc, _, _, _) in
            let listVC = vc as! CaptureListVC
            
            listVC.onCloseCaptureListVC = { [weak self] in self?.baseBackTo() }
            listVC.onShowColorDetail = { [weak self] color in self?.showColorDetailVC(with: color) }
        }
    }
    
    private func showColorDetailVC(with color: UIColor?) {
        registerChildFlowController(di.resolve(ColorFC.self, argument: baseFlow)!, fcName: ColorFC.name)
        
        flowDiveIn(ColorFC.name, vcName: ColorDetailVC.name, configure: { (vc, _, _, _) in
            guard let vm = (vc as? ColorDetailVC)?.viewModel as? ColorDetailVMP else { return }
            
            vm.color.value = color
        })
    }
}
