//
//  CaptureListVC.swift
//  eyeColors
//
//  Created by Martin Muller on 12.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

import RxSwift
import PeaceKit
import PeaceUIKit

import Swinject
import Closures

class CaptureListVC: ColorfulVC, ColorDetailPresentingP {
    // MARK: Outlets - Views
    @IBOutlet var tableView: UITableView!
    @IBOutlet var viewCapture: UIView!
    
    // MARK: Outlets - Components (lbl = Label, view = View, btn = Button, fld = TextField, ..)
    @IBOutlet var constraintTopViewCapture: NSLayoutConstraint!
    
    // MARK: Variables - UI
    
    // MARK: Constants
    fileprivate let viewHeaderSuggested = HeaderView(title: "Suggested")
    
    // MARK: Variables
    var selectedColor: UIView?
    
    fileprivate var topScrollOffset: CGPoint?
    fileprivate var colorList: [UIColor] = []
    fileprivate let viewTableHeader = CaptureListHeaderView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 88))
    
    fileprivate lazy var gesturePan = { return UIPanGestureRecognizer(target: self, action: #selector(handleViewPanned(recognizer:))) }()
    
    // MARK: Callbacks (on...)
    var onCloseCaptureListVC: VoidBlock = { }
    var onShowColorDetail: (UIColor) -> () = { _ in }
    var onScrollOverTop: (UIPanGestureRecognizer) -> () = { _ in }
    
    // MARK: Local Actions (handle...)
    private func handle(colorList: [UIColor]) {
        self.colorList = colorList
        
        DispatchQueue.main.async { [unowned self] in
            self.tableView.reloadData()
        }
    }
    
    // MARK: Navigation Actions (handle...)
    @objc private func handleViewPanned(recognizer: UIPanGestureRecognizer) {
        onScrollOverTop(recognizer)
    }
    
    // MARK: Internal Actions (do...)
    
    // MARK: Setup
    override func defineViewModel(_ di: Container) -> PeaceVMP { return di.resolve(CaptureListVM.self)! }
    
    // MARK: Bind to View Model
    override func bindToViewModel(_ vm: PeaceVMP) {
        let bvm = vm as! CaptureListVMP
        
        (bvm.colorList --> { [weak self] colorList in
            self?.view.backgroundColor = colorList[0]
//            self?.setNeedsStatusBarAppearanceUpdate()
            self?.handle(colorList: colorList) }) ~> rx.disposeBag
    }
    
    override func configureInput(_ vm: PeaceVMP) {
        let bvm = vm as! CaptureListVMP
        
    }
    
    // MARK: Bind local UI
    
    // MARK: Setup UI
    override func setupUI() {
        super.setupUI()

        gesturePan.delegate = self
        view.addGestureRecognizer(gesturePan)
        
        viewCapture.roundCorners([.topLeft, .topRight], radius: 40)
        tableView.roundCorners([.topLeft, .topRight], radius: 40)
        viewCapture.layer.applySketchShadow(alpha: 0.1, blur: 10)

        viewTableHeader.btnClose.onTap { [weak self] in self?.onCloseCaptureListVC() }
        tableView.tableFooterView = UIView()
        tableView.register(ColorListCell.name)
    }
    
    // MARK: View Lifecycle
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return (view.backgroundColor?.isDark ?? true) ? .lightContent : .default
    }
    
    // MARK: Syntax Sugar
    //private func viewModel() -> TaskEditVMP { return self.viewModel as! TaskEditVMP }
}

extension CaptureListVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int { return 2 }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0: return viewTableHeader.frame.height
        case 1: return viewHeaderSuggested.frame.height
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0: return viewTableHeader
        case 1: return viewHeaderSuggested
        default: return nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return colorList.count }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ColorListCell.name) as! ColorListCell
        cell.configure(with: colorList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedColor = (tableView.cellForRow(at: indexPath) as? ColorListCell)?.viewColor
        
        switch indexPath.section {
        case 0:
            onShowColorDetail(colorList[indexPath.row])
        case 1: ()
        default: ()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension CaptureListVC {
    var viewColor1: UIView? { return (tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ColorListCell)?.viewColor }
    var viewColor2: UIView? { return (tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? ColorListCell)?.viewColor }
    var viewColor3: UIView? { return (tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? ColorListCell)?.viewColor }
    var viewColor4: UIView? { return (tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as? ColorListCell)?.viewColor }
    var viewColor5: UIView? { return (tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as? ColorListCell)?.viewColor }
}

extension CaptureListVC: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        let gesture = (gestureRecognizer as! UIPanGestureRecognizer)
        let direction = gesture.velocity(in: view).y
        
        let y = constraintTopViewCapture.constant
        if (y == 0 && tableView.contentOffset.y == 0 && direction > 0) {
            tableView.isScrollEnabled = false
        } else {
            tableView.isScrollEnabled = true
        }
        
        return false
    }
}

infix operator -->; infix operator ==>; infix operator ~>; infix operator <->; infix operator +
