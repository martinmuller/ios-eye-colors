//
//  CaptureListPopingAnimationController.swift
//  eyeColors
//
//  Created by Martin Muller on 12.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

class CaptureListPopingAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    fileprivate var colorFromViewBackground: UIColor?
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval { return 0.3 }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: .from) as? CaptureListVC else { return }
        guard let toVC = transitionContext.viewController(forKey: .to) as? CaptureListPresentingVC else { return }
        
        guard let fromView = transitionContext.view(forKey: .from) else { return }
        guard let toView = transitionContext.view(forKey: .to) else { return }
        
        let container = transitionContext.containerView
        container.insertSubview(toView, belowSubview: fromView)
        
        colorFromViewBackground = fromVC.view.backgroundColor
        
        let viewFromBackground = UIView(frame: fromVC.view.positionIn(view: container))
        viewFromBackground.backgroundColor = fromVC.view.backgroundColor
        fromVC.view.backgroundColor = .clear
        container.insertSubview(viewFromBackground, belowSubview: fromView)
        
        fromVC.viewColor1?.isHidden = true
        fromVC.viewColor2?.isHidden = true
        fromVC.viewColor3?.isHidden = true
        fromVC.viewColor4?.isHidden = true
        fromVC.viewColor5?.isHidden = true
        
        let containerColor1 = UIView()
        if let color1 = fromVC.viewColor1 {
            containerColor1.frame = color1.positionIn(view: container)
            containerColor1.backgroundColor = color1.backgroundColor
            containerColor1.layer.cornerRadius = color1.layer.cornerRadius
            container.addSubview(containerColor1)
        }
        
        let containerColor2 = UIView()
        if let color2 = fromVC.viewColor2 {
            containerColor2.frame = color2.positionIn(view: container)
            containerColor2.backgroundColor = color2.backgroundColor
            containerColor2.layer.cornerRadius = color2.layer.cornerRadius
            container.addSubview(containerColor2)
        }
        
        let containerColor3 = UIView()
        if let color3 = fromVC.viewColor3 {
            containerColor3.frame = color3.positionIn(view: container)
            containerColor3.backgroundColor = color3.backgroundColor
            containerColor3.layer.cornerRadius = color3.layer.cornerRadius
            container.addSubview(containerColor3)
        }
        
        let containerColor4 = UIView()
        if let color4 = fromVC.viewColor4 {
            containerColor4.frame = color4.positionIn(view: container)
            containerColor4.backgroundColor = color4.backgroundColor
            containerColor4.layer.cornerRadius = color4.layer.cornerRadius
            container.addSubview(containerColor4)
        }
        
        let containerColor5 = UIView()
        if let color5 = fromVC.viewColor5 {
            containerColor5.frame = color5.positionIn(view: container)
            containerColor5.backgroundColor = color5.backgroundColor
            containerColor5.layer.cornerRadius = color5.layer.cornerRadius
            container.addSubview(containerColor5)
        }
        
        UIView.animateKeyframes(withDuration: transitionDuration(using: transitionContext), delay: 0.0, options: .allowUserInteraction, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.25, animations: {
                viewFromBackground.backgroundColor = .clear
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.25, animations: {
                fromView.alpha = 0.0
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 1.0, animations: {
                fromVC.constraintTopViewCapture.constant = fromView.frame.height
                fromView.layoutIfNeeded()
                
                containerColor1.frame = toVC.color1?.positionIn(view: container) ?? .zero
                containerColor1.layer.cornerRadius = toVC.color1?.layer.cornerRadius ?? 0
                
                containerColor2.frame = toVC.color2?.positionIn(view: container) ?? .zero
                containerColor2.layer.cornerRadius = toVC.color2?.layer.cornerRadius ?? 0
                
                containerColor3.frame = toVC.color3?.positionIn(view: container) ?? .zero
                containerColor3.layer.cornerRadius = toVC.color3?.layer.cornerRadius ?? 0
                
                containerColor4.frame = toVC.color4?.positionIn(view: container) ?? .zero
                containerColor4.layer.cornerRadius = toVC.color4?.layer.cornerRadius ?? 0
                
                containerColor5.frame = toVC.color5?.positionIn(view: container) ?? .zero
                containerColor5.layer.cornerRadius = toVC.color5?.layer.cornerRadius ?? 0
            })
        }) { [weak self] (_) in
            let success = !transitionContext.transitionWasCancelled
            if !success {
                toView.removeFromSuperview()
                
                fromVC.viewColor1?.isHidden = false
                fromVC.viewColor2?.isHidden = false
                fromVC.viewColor3?.isHidden = false
                fromVC.viewColor4?.isHidden = false
                fromVC.viewColor5?.isHidden = false
                
                fromVC.view.backgroundColor = self?.colorFromViewBackground
                fromVC.constraintTopViewCapture.constant = 0
            }
            else {
                toVC.color1?.isHidden = false
                toVC.color2?.isHidden = false
                toVC.color3?.isHidden = false
                toVC.color4?.isHidden = false
                toVC.color5?.isHidden = false
            }
            
            viewFromBackground.removeFromSuperview()
            
            containerColor1.removeFromSuperview()
            containerColor2.removeFromSuperview()
            containerColor3.removeFromSuperview()
            containerColor4.removeFromSuperview()
            containerColor5.removeFromSuperview()
            
            transitionContext.completeTransition(success)
        }
    }
}
