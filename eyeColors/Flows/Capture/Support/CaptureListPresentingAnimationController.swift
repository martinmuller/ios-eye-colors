//
//  CaptureListPresentingAnimationController.swift
//  eyeColors
//
//  Created by Martin Muller on 12.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

typealias CaptureListPresentingVC = ColorfulVC & CaptureListPresentingP
protocol CaptureListPresentingP {
    var color1: ColorButton? { get }
    var color2: ColorButton? { get }
    var color3: ColorButton? { get }
    var color4: ColorButton? { get }
    var color5: ColorButton? { get }
}

class CaptureListPresentingAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval { return 0.5 }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: .from) as? CaptureListPresentingVC else { return }
        guard let toVC = transitionContext.viewController(forKey: .to) as? CaptureListVC else { return }
        
        guard let fromView = transitionContext.view(forKey: .from) else { return }
        guard let toView = transitionContext.view(forKey: .to) else { return }
        
        let container = transitionContext.containerView
        
        toView.alpha = 0.0
        container.addSubview(toView)
        
        toView.frame = CGRect(x: 0, y: toView.frame.height, width: toView.frame.width, height: toView.frame.height)
        
        fromVC.color1?.isHidden = true
        fromVC.color2?.isHidden = true
        fromVC.color3?.isHidden = true
        fromVC.color4?.isHidden = true
        fromVC.color5?.isHidden = true
        
        let containerColor1 = UIView()
        if let color1 = fromVC.color1 {
            containerColor1.frame = color1.positionIn(view: container)
            containerColor1.backgroundColor = color1.backgroundColor
            containerColor1.layer.cornerRadius = color1.layer.cornerRadius
            container.addSubview(containerColor1)
        }
        
        let containerColor2 = UIView()
        if let color2 = fromVC.color2 {
            containerColor2.frame = color2.positionIn(view: container)
            containerColor2.backgroundColor = color2.backgroundColor
            containerColor2.layer.cornerRadius = color2.layer.cornerRadius
            container.addSubview(containerColor2)
        }
        
        let containerColor3 = UIView()
        if let color3 = fromVC.color3 {
            containerColor3.frame = color3.positionIn(view: container)
            containerColor3.backgroundColor = color3.backgroundColor
            containerColor3.layer.cornerRadius = color3.layer.cornerRadius
            container.addSubview(containerColor3)
        }
        
        let containerColor4 = UIView()
        if let color4 = fromVC.color4 {
            containerColor4.frame = color4.positionIn(view: container)
            containerColor4.backgroundColor = color4.backgroundColor
            containerColor4.layer.cornerRadius = color4.layer.cornerRadius
            container.addSubview(containerColor4)
        }
        
        let containerColor5 = UIView()
        if let color5 = fromVC.color5 {
            containerColor5.frame = color5.positionIn(view: container)
            containerColor5.backgroundColor = color5.backgroundColor
            containerColor5.layer.cornerRadius = color5.layer.cornerRadius
            container.addSubview(containerColor5)
        }
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .allowUserInteraction, animations: {
            toView.frame = CGRect(x: 0, y: 0, width: toView.frame.width, height: toView.frame.height)
            toView.alpha = 1.0
            
            containerColor1.frame = toVC.viewColor1?.positionIn(view: container) ?? .zero
            containerColor1.layer.cornerRadius = toVC.viewColor1?.layer.cornerRadius ?? 0
            
            containerColor2.frame = toVC.viewColor2?.positionIn(view: container) ?? .zero
            containerColor2.layer.cornerRadius = toVC.viewColor2?.layer.cornerRadius ?? 0
            
            containerColor3.frame = toVC.viewColor3?.positionIn(view: container) ?? .zero
            containerColor3.layer.cornerRadius = toVC.viewColor3?.layer.cornerRadius ?? 0
            
            containerColor4.frame = toVC.viewColor4?.positionIn(view: container) ?? .zero
            containerColor4.layer.cornerRadius = toVC.viewColor4?.layer.cornerRadius ?? 0
            
            containerColor5.frame = toVC.viewColor5?.positionIn(view: container) ?? .zero
            containerColor5.layer.cornerRadius = toVC.viewColor5?.layer.cornerRadius ?? 0
        }) { (_) in
            let success = !transitionContext.transitionWasCancelled
            if !success {
                toView.removeFromSuperview()
            }
            else {
                toVC.viewColor1?.isHidden = false
                toVC.viewColor2?.isHidden = false
                toVC.viewColor3?.isHidden = false
                toVC.viewColor4?.isHidden = false
                toVC.viewColor5?.isHidden = false
            }
            
            containerColor1.removeFromSuperview()
            containerColor2.removeFromSuperview()
            containerColor3.removeFromSuperview()
            containerColor4.removeFromSuperview()
            containerColor5.removeFromSuperview()
            
            transitionContext.completeTransition(success)
        }
    }
}
