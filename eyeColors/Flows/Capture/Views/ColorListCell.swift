//
//  ColorListCell.swift
//  eyeColors
//
//  Created by Martin Muller on 24/10/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

import PeaceUIKit

class ColorListCell: UITableViewCell {
    @IBOutlet var viewColor: UIView!
    @IBOutlet var lblHex: UILabel!
    @IBOutlet var lblRgb: UILabel!
    @IBOutlet var lblHsb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewColor.isHidden = true
        
        let viewBackground = UIView()
        viewBackground.backgroundColor = UIColor(r: 249, g: 249, b: 255)
        selectedBackgroundView = viewBackground
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        let color = viewColor.backgroundColor
        super.setHighlighted(highlighted, animated: animated)
        
        highlighted ? viewColor.backgroundColor = color : ()
        
        if highlighted {
            UIView.animate(withDuration: 0.15) { [unowned self] in
                self.viewColor.transform = self.viewColor.transform.scaledBy(x: 1.1, y: 1.1)
            }
        }
        else {
            UIView.animate(withDuration: 0.15) { [unowned self] in
                self.viewColor.transform = .identity
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        let color = viewColor.backgroundColor
        super.setSelected(selected, animated: animated)
        
        selected ? viewColor.backgroundColor = color : ()
    }
    
    public func configure(with color: UIColor) {
        viewColor.backgroundColor = color
        
        lblHex.text = color.toHexString()
        
        if let rgbComponents = color.colorComponents {
            lblRgb.text = "r:\(rgbComponents.red) g:\(rgbComponents.green) b:\(rgbComponents.blue)" }
    }
}
