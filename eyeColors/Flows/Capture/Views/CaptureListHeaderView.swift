//
//  CaptureListHeaderView.swift
//  eyeColors
//
//  Created by Martin Muller on 12.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

class CaptureListHeaderView: UIView {
    @IBOutlet private var contentView: UIView!
    @IBOutlet var btnClose: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        Bundle.main.loadNibNamed(CaptureListHeaderView.name, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        layoutIfNeeded()
    }
}
