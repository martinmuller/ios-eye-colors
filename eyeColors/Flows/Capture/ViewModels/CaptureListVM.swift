//
//  CaptureListVC.swift
//  eyeColors
//
//  Created by Martin Muller on 12.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import Foundation

import PeaceKit

import RxCocoa
import RxSwift

import Swinject

// MARK: Public View Model Interface
protocol CaptureListVMP: PeaceVMP {
    // MARK: Input
    var colorList: Variable<[UIColor]> { get }
    
    // MARK: Output
    
    // MARK: Methods
}

class CaptureListVM: ColorfulVM, CaptureListVMP {
    // MARK: Enums
    
    // MARK: Constants
    
    // MARK: Input
    let colorList: Variable<[UIColor]> = Variable([])
    
    // MARK: Output
    
    // MARK: Public Methods
    
    override func cleanUp() {
        
    }
    
    // MARK: Protected Methods
    override func defineModels(_ di: Container) {
//        models[ColorModel.name] = di.resolve(ColorModel.self)
    }
    
    override func bindOutputToModels(_: [String: PeaceModelP]) {

    }
    
    override func configureInput() {

    }
    
    // MARK: Private Methods
    
    // MARK: Syntax Sugar
    //private func model() -> TaskModelP { return self.models[modelTask] as! TaskModelP }
    private var colorModel: ColorModel {
        get { return models[ColorModel.name] as! ColorModel }
        // TODO: Legal?
        //        set { models[ColorModel.name] = newValue }
    }
}

// Peace Rx Operators
infix operator -->; infix operator ==>; infix operator ~>; infix operator <->; infix operator +
