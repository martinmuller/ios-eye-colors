//
//  ColorfulFC.swift
//  eyeColors
//
//  Created by Martin Muller on 08.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

import PeaceKit

open class ColorfulFC: PeaceFC {
    public func register<VM: PeaceVM>(vm: VM.Type) {
        di.register(vm) { [unowned self] (_) -> VM in
            vm.init(diProvider: self) }
    }
    
    public func register<Model: PeaceModel>(model: Model.Type) {
        di.register(model) { [unowned self] (_) -> Model in
            model.init(diProvider: self) }.inObjectScope(.container)
    }
    
    public func register<Service: PeaceServiceP>(service: Service.Type) {
        di.register(service) { (_) -> Service in
            service.init() }.inObjectScope(.container)
    }
    
    public func register<FC: PeaceFC>(fc: FC.Type) {
        di.register(fc) { [unowned self] (_) -> FC in
            fc.init(ac: self.ac) }
    }
}
