//
//  AppDelegate.swift
//  eyeColors
//
//  Created by Martin Muller on 17.03.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    let applicationController = ColorfulAC()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupServices()
        setupGlobalAppearance()
        prepareWindow()
        setupRootVC()
        
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        UserDefaults.standard.synchronize()
    }
    
    func setupServices() {
        applicationController.setupServices()
    }

    func setupRootVC() {
        applicationController.setupRootFlow()
        
        guard let window = window else { return }
        var rootVC: UIViewController?
        rootVC = applicationController.createRootVCinNVC()
        window.rootViewController = rootVC

        window.makeKeyAndVisible()
    }
    
    // MARK: Appearance
    func setupGlobalAppearance() {
//        let navBarAppearance = UINavigationBar.appearance()
//        let tabBarAppearance = UITabBar.appearance()
//        let tabBarItemAppearance = UITabBarItem.appearance()
//        let barButtonItemAppearance = UIBarButtonItem.appearance()
//        let segmentedControlAppearance = UISegmentedControl.appearance()
//
//        navBarAppearance.setTitleAttributes(.white, font: R.font.muktaSemiBold(size: 20))
//        navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white, .font: R.font.muktaSemiBold(size: 34)!]
//
//        tabBarItemAppearance.setTitleTextAttributes([.font: R.font.muktaSemiBold(size: 12)!, .foregroundColor: UIColor.tiHeather, .strokeColor: UIColor.tiHeather], for: .normal)
//        tabBarItemAppearance.setTitleTextAttributes([.font: R.font.muktaSemiBold(size: 12)!, .foregroundColor: UIColor.tiPurpleyBlue, .strokeColor: UIColor.tiPurpleyBlue], for: .selected)
//
//        //        tabBarAppearance.isTranslucent = false
//
//        //        barButtonItemAppearance.tintColor = .white
//        barButtonItemAppearance.setTitleTextAttributes([.font : R.font.muktaRegular(size: 19)!], for: .normal)
//        barButtonItemAppearance.setTitleTextAttributes([.font : R.font.muktaRegular(size: 19)!], for: .highlighted)
//        barButtonItemAppearance.setTitleTextAttributes([.font : R.font.muktaRegular(size: 19)!], for: .disabled)
//
//
//        segmentedControlAppearance.setTitleTextAttributes([NSAttributedStringKey.font : R.font.muktaSemiBold(size: 15)], for: .normal)
//        segmentedControlAppearance.setTitleTextAttributes([NSAttributedStringKey.font : R.font.muktaSemiBold(size: 15)], for: .highlighted)
//        segmentedControlAppearance.setTitleTextAttributes([NSAttributedStringKey.font : R.font.muktaSemiBold(size: 15)], for: .disabled)
    }
    
    // MARK: Private Methods
    func prepareWindow() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
    }
}

