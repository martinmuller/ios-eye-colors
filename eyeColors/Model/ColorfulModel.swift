//
//  ColorfulModel.swift
//  eyeColors
//
//  Created by Martin Muller on 09.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import Foundation

import PeaceKit
import Swinject

class ColorfulModel: PeaceModel {
    override func setupDI(_ di: Container) {
        super.setupDI(di)
        
    }
    
    override func defineServices(_ di: Container) {
        super.defineServices(di)
    }

    
    override func defineApiService() {

    }
    
    override func isApiDefined() -> Bool { return false }
    
    // For larger apps, flow-wide models can be defined as childs of app-wide model...
    
    // MARK: Protected Methods - HTTP Headers Setup
    
    // MARK: Public functions
    
    // MARK: Simple request
}
