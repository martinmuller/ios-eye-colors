//
//  CaptureModel.swift
//  eyeColors
//
//  Created by Martin Muller on 23/10/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import Swinject
import PeaceKit

import RxSwift
import RxCocoa
import NSObject_Rx

import RealmSwift
//import RealmTypeSafeQuery

protocol CaptureModelP {
    // MARK: Input
    var requestStartCamera: Driver<Void> { get set }
    var requestStopCamera: Driver<Void> { get set }
    var requestZoomVelocity: Driver<CGFloat> { get set }
    var requestColorFromPosition: Driver<CGPoint> { get set }
    
    // MARK: Output
    var cameraOutput: Driver<UIImage> { get }
    var colorsFromOutput: Driver<[UIColor]> { get }
    var colorAtPosition: Driver<UIColor> { get }
    
    // MARK: Methods
}

class CaptureModel: ColorfulModel, CaptureModelP {
    // MARK: Services
    private var serviceCamera: CameraService!
    private var serviceColor: ColorService!
    
    // MARK: Pagination requests
    
    // MARK: Constants
    
    // MARK: Enums
    
    // MARK: Input
    var requestStartCamera: Driver<Void> = Driver.never()
    var requestStopCamera: Driver<Void> = Driver.never()
    var requestZoomVelocity: Driver<CGFloat> = Driver.never()
    var requestColorFromPosition: Driver<CGPoint> = Driver.never()
    
    // MARK: Output - Drivers
    private(set) var cameraOutput: Driver<UIImage> = Driver.never()
    private(set) var colorsFromOutput: Driver<[UIColor]> = Driver.never()
    private(set) var colorAtPosition: Driver<UIColor> = Driver.never()
    
    // MARK: Private Pipes - Pipes
    private var cameraCIImageOutput: Driver<CIImage> = Driver.never()
    
    // MARK: Bindings
    override func defineOutputBindings() {
        super.defineOutputBindings()
        
        serviceCamera.cameraOutput.outputDriver ==> cameraOutput
        cameraOutput.throttle(1).map { [unowned self] image in self.configureColors(from: image) }.switchLatest() ==> colorsFromOutput
    }
    
    override func configureInput() {
        serviceCamera.cameraCIImageOutput.outputDriver ==> cameraCIImageOutput
        
        (requestStartCamera --> { [weak self] in self?.serviceCamera.startCamera() }) ~> rx.disposeBag
        (requestStopCamera --> { [weak self] in self?.serviceCamera.stopCamera() }) ~> rx.disposeBag
        (requestZoomVelocity --> { [weak self] zoomVelocity in self?.serviceCamera.handle(zoomVelocity: zoomVelocity) }) ~> rx.disposeBag
        
        configureColor(at: requestColorFromPosition, from: cameraCIImageOutput) ==> colorAtPosition
    }
    
    private func configureColors(from image: UIImage) -> Driver<[UIColor]> {
        return Observable.create { [unowned self] (observer) -> Disposable in
            self.serviceColor.colors(from: image, completion: { (colors) in
                observer.onNext(colors)
            })
            
            return Disposables.create()
            }
            .asDriver(onErrorJustReturn: [])
    }
    
    private func configureColor(at position: Driver<CGPoint>, from ciImage: Driver<CIImage>) -> Driver<UIColor> {
        return Driver.combineLatest(position,
                                    position.withLatestFrom(ciImage))
            .debounce(0.1)
            .map({ [weak self] point, ciImage in
                return self?.color(from: ciImage, at: point)
            })
            .filter { $0 != nil }
            .map { $0! }
    }
    
    private func color(from ciImage: CIImage, at point: CGPoint) -> UIColor? {
        let startCrop = Date()
        let cropRectSize: CGFloat = 100
        let imageCropped = ciImage.cropped(to: CGRect(x: point.x - (cropRectSize / 2), y: point.y - (cropRectSize / 2), width: cropRectSize, height: cropRectSize))
        
        guard let uiImageCropped = serviceCamera.imageFrom(ciImage: imageCropped) else { return nil }
        
        let elapsedCropped = -startCrop.timeIntervalSinceNow
        NSLog("time for crop: \(Int(elapsedCropped * 1000.0))ms")
        
        let start = Date()
        let dominantColor = serviceColor.dominantColor(from: uiImageCropped)
        
        let elapsed = -start.timeIntervalSinceNow
        NSLog("time for getColorFromImage: \(Int(elapsed * 1000.0))ms")
        
        return dominantColor
    }
    
    // MARK: Setup
    override func defineServices(_ di: Container) {
        super.defineServices(di)
        
        serviceCamera = di.resolve(CameraService.self)!
        serviceColor = di.resolve(ColorService.self)!
    }
}
