//
//  ColorModel.swift
//  eyeColors
//
//  Created by Martin Muller on 09.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import Foundation

import Swinject
import PeaceKit

import RxSwift
import RxCocoa
import NSObject_Rx

import RealmSwift
//import RealmTypeSafeQuery

protocol ColorModelP {
    // MARK: Input
    var requestSaveColor: Driver<Color> { get set }
    
    // MARK: Output
    var resultColorList: Driver<ChangesetResult<Color>> { get }
    
    // MARK: Methods
}

class ColorModel: ColorfulModel, ColorModelP {
    // MARK: Services
    private var serviceRealm: RealmService!
    
    // MARK: Pagination requests
    
    // MARK: Constants
    
    // MARK: Enums
    
    // MARK: Input
    var requestSaveColor: Driver<Color> = Driver.never()
    
    // MARK: Output - Drivers
    private(set) var resultColorList: Driver<ChangesetResult<Color>> = Driver.never()
    
    // MARK: Bindings
    override func defineOutputBindings() {
        super.defineOutputBindings()
        
        serviceRealm.objects(from: Color.self).reactifyChangeset() ==> resultColorList
    }
    
    override func configureInput() {
        
        // TODO: Improve
        serviceRealm.add(pipe: requestSaveColor.map { Result.resultSuccess(item: $0) })
    }
    
    // MARK: Setup
    override func defineServices(_ di: Container) {
        super.defineServices(di)
        
        serviceRealm = di.resolve(RealmService.self)
    }
}
