//
//  Color.swift
//  eyeColors
//
//  Created by Martin Muller on 23/10/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

import PeaceUIKit
import RealmSwift

class Color: Object {
    @objc dynamic var hex = ""
    
    convenience init(hex: String) {
        self.init()
        
        self.hex = hex
    }
    
    convenience init(color: UIColor) {
        self.init()
        
        self.hex = color.toHexString()
    }
    
    override public static func primaryKey() -> String? { return "hex" }
}
