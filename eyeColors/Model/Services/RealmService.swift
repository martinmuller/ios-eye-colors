//
//  RealmService.swift
//  eyeColors
//
//  Created by Martin Muller on 23/10/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import PeaceKit

import Realm
import RealmSwift

import RxSwift
import RxCocoa
import RxRealm

class RealmService: NSObject, PeaceServiceP {
    private var realm: Realm!
    
    private(set) var realmSize: Driver<UInt64> = Driver.never()
    
    // MARK: Setup
    public override required init() {
        super.init()
        
        realm = try! Realm(configuration: resolveConfig())
        
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        
        realmSize = configureRealmSize()
    }
    
    private func resolveConfig() -> Realm.Configuration {
        let config = Realm.Configuration(schemaVersion: 1, migrationBlock: { _, _ in })
        return config
    }
    
    private func configureRealmSize() -> Driver<UInt64> {
        return Observable.deferred { () -> Observable<UInt64?> in
            var size: Any?
            do {
                size = try FileManager.default.attributesOfItem(atPath: Realm.Configuration.defaultConfiguration.fileURL!.path)[FileAttributeKey.size]
            } catch (let error) {
                print("File size error: \(error)")
                return Observable.just(nil)
            }
            guard let fileSize = size as? UInt64 else { return Observable.just(nil) }
            
            return Observable.just(fileSize)
            }
            .asDriver(onErrorJustReturn: nil)
            .filter { $0 != nil }
            .map { $0! }
    }
    
    // MARK: Generic - simple
    public func objects<T: Object>(from: T.Type) -> Results<T> { return realm.objects(from) }
    public func deleteAll() { try! realm.write { realm.deleteAll() } }
    public func invalidate() { try! realm.write { realm.invalidate() } }
    func compactRealm() {
        let defaultURL = Realm.Configuration.defaultConfiguration.fileURL!
        let defaultParentURL = defaultURL.deletingLastPathComponent()
        let compactedURL = defaultParentURL.appendingPathComponent("default-compact.realm")
        
        autoreleasepool {
            let realm = try! Realm(configuration: resolveConfig())
            try! realm.writeCopy(toFile: compactedURL)
        }
        try! FileManager.default.removeItem(at: defaultURL)
        try! FileManager.default.moveItem(at: compactedURL, to: defaultURL)
    }
    
    private func add<T: Object>(_ object: T, update: Bool = false) { try! realm.write { realm.add(object, update: update) } }
    private func remove<T: Object>(_ object: T) { try! realm.write { realm.delete(object) } }
    
    // MARK: Add
    public func add<T: Object>(pipe: Driver<T?>?, update: Bool = true) {
        guard let pipe = pipe else { return }
        
        pipe.filter { $0 != nil }.map { $0! }
            .drive(realm.rx.add(update: update)) ~> rx.disposeBag
    }
    
    public func add<T: Object>(pipe: Driver<Result<[T]>>?, update: Bool = true) {
        guard let pipe = pipe else { return }
        
        pipe.filter { $0.ok == .success && $0.item != nil }
            .map { $0.item! }
            .drive(realm.rx.add(update: update)) ~> rx.disposeBag
    }
    
    public func add<T: Object>(pipe: Driver<Result<T>>?, update: Bool = true) {
        guard let pipe = pipe else { return }
        
        pipe.filter { $0.ok == .success && $0.item != nil }
            .map { $0.item! }
            .drive(realm.rx.add(update: update)) ~> rx.disposeBag
    }

    // MARK: Remove
    public func remove<T: Object>(pipe: Driver<Result<T>>?) {
        pipe?
            .filter { $0.ok == .success && $0.item != nil }
            .map { $0.item! }
            .filter { !$0.isInvalidated }
            .drive(realm.rx.delete()) ~> rx.disposeBag
    }
    
    public func remove<T: Object>(pipe: Driver<Result<[T]>>?) {
        pipe?
            .filter { $0.ok == .success && $0.item != nil }
            .map { $0.item! }
            .drive(realm.rx.delete()) ~> rx.disposeBag
    }
    
    public func remove<T: Object>(_ object: T.Type, includeIDs: [Int]) {
        let objectsToRemove = objects(from: T.self).filter(NSPredicate(format: "id IN %@", includeIDs))
        
        if !objectsToRemove.isInvalidated {
            try! realm.write {
                realm.delete(objectsToRemove)
            }
        }
    }
}
