//
//  CameraService.swift
//  eyeColors
//
//  Created by Martin Muller on 08.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import AVFoundation

import PeaceKit

import RxSwift
import RxCocoa

class CameraService: NSObject, AVCaptureVideoDataOutputSampleBufferDelegate, PeaceServiceP {
    // MARK: Running variables
    private let captureSession = AVCaptureSession()
    private let context = CIContext()
    private let sessionQueue = DispatchQueue(label: "session_queue")
    
    // MARK Constants
    private let mediaType = AVMediaType.video
    private let position = AVCaptureDevice.Position.back
    private let quality = AVCaptureSession.Preset.high
    
    // MARK: Variables
    private var permissionGranted = false
    private var currentCaptureDevice: AVCaptureDevice?
    
    // MARK: Public output
    private(set) var cameraOutput = Action<UIImage>()
    private(set) var cameraCIImageOutput = Action<CIImage>()
    
    // MARK: Setup
    override required init() {
        super.init()
        
        requestAuthorizationIfNeeded()
        sessionQueue.async { [unowned self] in
            self.configureSession()
//            self.captureSession.startRunning()
        }
    }
    
    // MARK: Private methods
    private func requestAuthorization() {
        sessionQueue.suspend()
        AVCaptureDevice.requestAccess(for: mediaType) { [unowned self] granted in
            self.permissionGranted = granted
            self.sessionQueue.resume()
        }
    }
    
    private func configureSession() {
        guard permissionGranted else { return }
        captureSession.sessionPreset = quality
        
        guard let captureDevice = selectCaptureDevice() else { return }
        guard let captureDeviceInput = try? AVCaptureDeviceInput(device: captureDevice) else { return }
        
        currentCaptureDevice = captureDevice
    
        guard captureSession.canAddInput(captureDeviceInput) else { return }
        captureSession.addInput(captureDeviceInput)
        
        let videoOutput = AVCaptureVideoDataOutput()
        videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "sample buffer"))
        guard captureSession.canAddOutput(videoOutput) else { return }
        captureSession.addOutput(videoOutput)
        
        guard let connection = videoOutput.connection(with: mediaType) else { return }
        guard connection.isVideoOrientationSupported else { return }
        guard connection.isVideoMirroringSupported else { return }
        connection.videoOrientation = .portrait
        connection.isVideoMirrored = position == .front
    }
    
    private func imageFrom(sampleBuffer: CMSampleBuffer) -> UIImage? {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return nil }
        let ciImage = CIImage(cvPixelBuffer: imageBuffer)
        guard let cgImage = context.createCGImage(ciImage, from: ciImage.extent) else { return nil }
        return UIImage(cgImage: cgImage)
    }
    
    private func ciImageFrom(sampleBuffer: CMSampleBuffer) -> CIImage? {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return nil }
        return CIImage(cvPixelBuffer: imageBuffer)
    }
    
    private func selectCaptureDevice() -> AVCaptureDevice? {
        return AVCaptureDevice.DiscoverySession.init(deviceTypes: [.builtInWideAngleCamera], mediaType: mediaType, position: position).devices.first
    }
    
    // MARK: Delegate
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let ciImage = ciImageFrom(sampleBuffer: sampleBuffer) else { return }
        DispatchQueue.main.async { [unowned self] in
            self.cameraCIImageOutput.trigger(with: ciImage)
        }
        
        guard let uiImage = imageFrom(ciImage: ciImage) else { return }
        DispatchQueue.main.async { [unowned self] in
            self.cameraOutput.trigger(with: uiImage)
        }
    }
    
    
    // MARK: Public methods
    public func requestAuthorizationIfNeeded() {
        switch AVCaptureDevice.authorizationStatus(for: mediaType) {
        case .authorized: permissionGranted = true
        case .notDetermined: requestAuthorization()
        default: permissionGranted = false
        }
    }
    
    public func startCamera() {
        sessionQueue.async { [unowned self] in
            if !self.captureSession.isRunning {
                self.captureSession.startRunning()
            }
        }
    }
    
    public func stopCamera() {
        sessionQueue.async { [unowned self] in
            if self.captureSession.isRunning {
                self.captureSession.stopRunning()
            }
        }
    }
    
    public func handle(zoomVelocity: CGFloat) {
        guard let device = currentCaptureDevice else { return }
        
        let maxZoomFactor = device.activeFormat.videoMaxZoomFactor
        let pinchVelocityDividerFactor: CGFloat = 10.0
        
        do {
            
            try device.lockForConfiguration()
            defer { device.unlockForConfiguration() }
            
            let desiredZoomFactor = device.videoZoomFactor + atan2(zoomVelocity, pinchVelocityDividerFactor)
            device.videoZoomFactor = max(1.0, min(desiredZoomFactor, maxZoomFactor))
            
        } catch {
            print(error)
        }
    }
    
    public func imageFrom(ciImage: CIImage) -> UIImage? {
        guard let cgImage = context.createCGImage(ciImage, from: ciImage.extent) else { return nil }
        return UIImage(cgImage: cgImage)
    }
}

infix operator -->; infix operator ==>; infix operator ~>; infix operator <->; infix operator +
