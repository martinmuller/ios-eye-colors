//
//  ColorService.swift
//  eyeColors
//
//  Created by Martin Muller on 11.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

import PeaceKit

class ColorService: NSObject, PeaceServiceP {
    // MARK: Running variables
    private let colorQueue = DispatchQueue(label: "color_queue")
    
    // MARK: Setup
    override required init() {
        super.init()
    }
    
    public func colors(from image: UIImage, completion: @escaping ([UIColor]) -> ()) {
        colorQueue.async {
            guard let colors = ColorThief.getPalette(from: image, colorCount: 5, quality: 10, ignoreWhite: true) else { return }
            let finalColors = colors.map { $0.makeUIColor() }
            
            DispatchQueue.main.async {
                completion(finalColors)
            }
        }
    }
    
    public func dominantColor(from image: UIImage) -> UIColor? {
        guard let dominantColor = ColorThief.getColor(from: image, quality: 1) else { return nil }
        return dominantColor.makeUIColor()
    }
}
