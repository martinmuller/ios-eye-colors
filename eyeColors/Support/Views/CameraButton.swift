//
//  CameraButton.swift
//  eyeColors
//
//  Created by Martin Muller on 10.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

class CameraButton: UIControl {
    
    var onTouch: VoidBlock = { }
    
    private var animatorUp = UIViewPropertyAnimator()
    private var animatorDown = UIViewPropertyAnimator()
    
    private let normalColor = UIColor("333333")
    private let highlightedColor = UIColor("737373")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    private func sharedInit() {
        
        backgroundColor = normalColor
    
        addTarget(self, action: #selector(touchUpCompleted), for: .touchUpInside)
        addTarget(self, action: #selector(touchDown), for: [.touchDown, .touchDragEnter])
        addTarget(self, action: #selector(touchUp), for: [.touchUpInside, .touchDragExit, .touchCancel])
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 64, height: 64)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.width / 2
        
        layer.shadowRadius = 6
        layer.shadowOpacity = 0.6
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowColor = normalColor.cgColor
    }
    
    @objc private func touchDown() {
        animatorUp.stopAnimation(true)
        backgroundColor = highlightedColor
        
        animatorDown = UIViewPropertyAnimator(duration: 0.15, curve: .easeIn, animations: {
            self.transform = self.transform.scaledBy(x: 0.9, y: 0.9)
            self.layer.shadowColor = UIColor.clear.cgColor
        })
        animatorDown.startAnimation()
    }
    
    @objc private func touchUp() {
        animatorUp = UIViewPropertyAnimator(duration: 0.5, curve: .easeOut, animations: {
            self.transform = .identity
            self.backgroundColor = self.normalColor
            self.layer.shadowColor = self.normalColor.cgColor
        })
        animatorUp.startAnimation()
    }
    
    @objc private func touchUpCompleted() {
        onTouch()
    }
}
