//
//  ColorButton.swift
//  eyeColors
//
//  Created by Martin Muller on 11.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import UIKit

class ColorButton: UIControl {
    private enum Position { case up, down }
    
    private var animatorUp = UIViewPropertyAnimator()
    private var animatorDown = UIViewPropertyAnimator()
    
    private var currentPosition: Position = .up
    private(set) var currentColor: UIColor?
    
    var onTouch: VoidBlock = { }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    private func sharedInit() {
        addTarget(self, action: #selector(touchUpCompleted), for: .touchUpInside)
        addTarget(self, action: #selector(touchDown), for: [.touchDown, .touchDragEnter])
        addTarget(self, action: #selector(touchUp), for: [.touchUpInside, .touchDragExit, .touchCancel])
        
        backgroundColor = .red
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 5
    }
    
    @objc private func touchDown() {
        currentPosition = .down
        
        animatorDown = UIViewPropertyAnimator(duration: 0.15, curve: .easeIn, animations: {
            self.transform = self.transform.scaledBy(x: 0.9, y: 0.9)
//            self.backgroundColor = self.currentColor?.withAlphaComponent(0.8)
        })
        
        animatorDown.startAnimation()
    }
    
    @objc private func touchUp() {
        currentPosition = .up
        
        animatorUp = UIViewPropertyAnimator(duration: 0.5, curve: .easeOut, animations: {
            self.transform = .identity
//            self.backgroundColor = self.currentColor
        })
        
        animatorUp.startAnimation()
    }
    
    @objc private func touchUpCompleted() {
        onTouch()
    }
    
    public func setBackgroundColor(_ color: UIColor, duration: TimeInterval = 0.2) {
        if currentPosition == .up {
            currentColor = color
            
            UIView.transition(with: self, duration: duration, animations: { [weak self] in
                self?.backgroundColor = color
            })
        }
    }
}
