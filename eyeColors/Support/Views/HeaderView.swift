//
//  HeaderView.swift
//  eyeColors
//
//  Created by Martin Muller on 24/10/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

class HeaderView: UIView {
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var lblTitle: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    convenience init(title: String?) {
        self.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 66))
        
        lblTitle.text = title
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        Bundle.main.loadNibNamed(HeaderView.name, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        layoutIfNeeded()
    }
}
