//
//  Constants.swift
//  eyeColors
//
//  Created by Martin Muller on 09.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import Foundation

typealias VoidBlock = () -> ()

typealias StringBlock = (String) -> ()
typealias StringOptBlock = (String?) -> ()

typealias IntBlock = (Int) -> ()
typealias IntOptBlock = (Int?) -> ()
