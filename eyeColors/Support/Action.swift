//
//  Action.swift
//  eyeColors
//
//  Created by Martin Muller on 08.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

public class Action<T> {
    private var _output: Observable<T?>!
    private(set) var onInput: (T) -> () = { _ in }
    private var onCompleted: () -> () = { }
    
    init(_ initialValue: T? = nil) {
        _output = Observable.create { [weak self] (observer) -> Disposable in
            self?.onInput = {
                value in observer.onNext(value)
            }
            self?.onCompleted = {
                observer.onCompleted()
            }
            return Disposables.create()
        }
        
        if let value = initialValue { onInput(value) }
    }
    
    public func trigger(with value: T) {
        onInput(value)
    }
    public func completed() { onCompleted() }
    
    public var outputDriver: Driver<T> {
        return _output
            .asDriver(onErrorJustReturn: nil)
            .filter { $0 != nil }
            .map { $0! }
    }
    
    public var output: Observable<T> {
        return _output
            .filter { $0 != nil }
            .map { $0! }
    }
}

extension Action where T == Void {
    public func trigger() {
        onInput(())
    }
}
