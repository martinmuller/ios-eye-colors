//
//  UIColor.swift
//  eyeColors
//
//  Created by Martin Muller on 23/10/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

extension UIColor {
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format:"#%06x", rgb)
    }
    
    var colorComponents: (red: Int, green: Int, blue: Int, alpha: Int)? {
        guard let components = self.cgColor.components else { return nil }
        
        return (
            red: Int(components[0] * 255),
            green: Int(components[1] * 255),
            blue: Int(components[2] * 255),
            alpha: Int(components[3] * 255)
        )
    }
}
