//
//  Results+Object.swift
//  eyeColors
//
//  Created by Martin Muller on 23/10/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa
import RxRealm

import RealmSwift
import PeaceKit

extension Results where Element: Object {
    public func reactifyCollection() -> Driver<Results<Element>> {
        return Observable.collection(from: self)
            .map { return Result.resultSuccess(item: $0) }
            .asDriver(onErrorJustReturn: Result.resultFailed())
            .filter { $0.item != nil }
            .map { $0.item! }
    }
    
    public func reactifyJust() -> Driver<Results<Element>> {
        return Observable.just(self)
            .map { return Result.resultSuccess(item: $0) }
            .asDriver(onErrorJustReturn: Result.resultFailed())
            .filter { $0.item != nil }
            .map { $0.item! }
    }
    
    public func reactifyChangeset() -> Driver<ChangesetResult<Element>> {
        return Observable.changeset(from: self)
            .map { return Result.resultSuccess(item: $0) }
            .asDriver(onErrorJustReturn: Result.resultFailed())
            .filter { $0.item != nil }
            .map { $0.item! }
    }
}

public typealias ChangesetResult<T: Object> = (collection: AnyRealmCollection<T>, changeset: RealmChangeset?)
