//
//  ColorfulAC.swift
//  eyeColors
//
//  Created by Martin Muller on 08.08.18.
//  Copyright © 2018 marta. All rights reserved.
//

import Foundation

import PeaceKit
import Swinject

import SwiftyUserDefaults

class ColorfulAC: PeaceAC {
    var transitionHandler: ColorFlowDelegateHandler?
    
    override func setupServices() {
//        Fabric.with([Crashlytics.self])
//        IQKeyboardManager.shared().isEnabled = true
    }
    
    override func createRootFlow(_ di: Container) {
        rootFlow = di.resolve(ColorFC.self)
    }
    
    override func setupDI(_ di: Container) {
        // Register top level Flow Controller(s)
        di.register(CaptureFC.self) { [unowned self] _, nvc in CaptureFC(name: "Capture", ac: self, baseFlow: nvc) }
        
        register(fc: ColorFC.self)
        di.register(ColorFC.self) { [unowned self] _, nvc in ColorFC(name: "Color", ac: self, baseFlow: nvc) }
        
        // Register app wide service(s)
        register(service: CameraService.self)
        register(service: ColorService.self)
        register(service: RealmService.self)
        
        // Register app wide model(s)
        
        // Register other Flow Controller(s)

    }
    
    // Create app-wide model(s) instances
    override func defineModels(_ di: Container) {
        
    }
}
